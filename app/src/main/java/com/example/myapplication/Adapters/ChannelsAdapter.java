package com.example.myapplication.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapplication.Model.Channel;
import com.example.myapplication.Model.City;
import com.example.myapplication.R;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class ChannelsAdapter  extends RecyclerView.Adapter<ChannelsAdapter.MyView> {

    // List with String type
    private List<Channel> list;

    // View Holder class which
    // extends RecyclerView.ViewHolder
    public class MyView
            extends RecyclerView.ViewHolder {

        // Text View
        TextView textView;
        ImageView imageView;

        // parameterised constructor for View Holder class
        // which takes the view as a parameter
        public MyView(View view)
        {
            super(view);

            // initialise TextView with id
            textView = (TextView)view
                    .findViewById(R.id.title);
            imageView = (ImageView)view
                    .findViewById(R.id.cover);

        }
    }

    // Constructor for adapter class
    // which takes a list of String type
    public ChannelsAdapter(List<Channel> horizontalList)
    {
        this.list = horizontalList;
    }

    // Override onCreateViewHolder which deals
    // with the inflation of the card layout
    // as an item for the RecyclerView.
    @Override
    public ChannelsAdapter.MyView onCreateViewHolder(ViewGroup parent,
                                                    int viewType)
    {

        // Inflate item.xml using LayoutInflator
        View itemView
                = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.latest_news_lyt,
                        parent,
                        false);

        // return itemView
        return new ChannelsAdapter.MyView(itemView);
    }

    // Override onBindViewHolder which deals
    // with the setting of different data
    // and methods related to clicks on
    // particular items of the RecyclerView.
    @Override
    public void onBindViewHolder(final ChannelsAdapter.MyView holder,
                                 final int position)
    {

        // Set the text of each item of
        // Recycler view with the list items
        holder.textView.setText(list.get(position).getTitle());
        holder.imageView.setImageDrawable(list.get(position).getDrawable());
    }

    // Override getItemCount which Returns
    // the length of the RecyclerView.
    @Override
    public int getItemCount()
    {
        return list.size();
    }
}

