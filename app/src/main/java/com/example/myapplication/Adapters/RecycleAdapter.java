package com.example.myapplication.Adapters;
  import android.view.View;
        import android.view.ViewGroup;
        import android.widget.TextView;
        import android.view.LayoutInflater;

  import com.example.myapplication.Model.City;
  import com.example.myapplication.R;

  import java.util.List;

  import androidx.recyclerview.widget.RecyclerView;

// The adapter class which
// extends RecyclerView Adapter
public class RecycleAdapter
        extends RecyclerView.Adapter<RecycleAdapter.MyView> {

    // List with String type
    private List<City> list;

    // View Holder class which
    // extends RecyclerView.ViewHolder
    public class MyView
            extends RecyclerView.ViewHolder {

        // Text View
        TextView textView1;
        TextView textView2;

        // parameterised constructor for View Holder class
        // which takes the view as a parameter
        public MyView(View view)
        {
            super(view);

            // initialise TextView with id
            textView1 = (TextView)view
                    .findViewById(R.id.degree);
            textView2 = (TextView)view
                    .findViewById(R.id.city_name);

        }
    }

    // Constructor for adapter class
    // which takes a list of String type
    public RecycleAdapter(List<City> horizontalList)
    {
        this.list = horizontalList;
    }

    // Override onCreateViewHolder which deals
    // with the inflation of the card layout
    // as an item for the RecyclerView.
    @Override
    public MyView onCreateViewHolder(ViewGroup parent,
                                     int viewType)
    {

        // Inflate item.xml using LayoutInflator
        View itemView
                = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.recycle_item,
                        parent,
                        false);

        // return itemView
        return new MyView(itemView);
    }

    // Override onBindViewHolder which deals
    // with the setting of different data
    // and methods related to clicks on
    // particular items of the RecyclerView.
    @Override
    public void onBindViewHolder(final MyView holder,
                                 final int position)
    {

        // Set the text of each item of
        // Recycler view with the list items
        holder.textView1.setText(list.get(position).getDegree()+"°");
        holder.textView2.setText(list.get(position).getName());
    }

    // Override getItemCount which Returns
    // the length of the RecyclerView.
    @Override
    public int getItemCount()
    {
        return list.size();
    }
}
