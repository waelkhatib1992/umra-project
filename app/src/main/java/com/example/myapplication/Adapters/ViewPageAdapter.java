package com.example.myapplication.Adapters;

import android.content.Context;

import com.example.myapplication.Views.HomeFragment;
import com.example.myapplication.Views.PackageFragment;
import com.example.myapplication.Views.ProfileFragment;
import com.example.myapplication.Views.SupportFragment;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class ViewPageAdapter extends FragmentPagerAdapter {

    private Context myContext;
    int totalTabs;

    public ViewPageAdapter(Context context, FragmentManager fm, int totalTabs) {
        super(fm);
        myContext = context;
        this.totalTabs = totalTabs;
    }

    // this is for fragment tabs
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                HomeFragment homeFragment = new HomeFragment();
                return homeFragment;
            case 1:
                PackageFragment packageFragment = new PackageFragment();
                return packageFragment;
            case 2:
                SupportFragment supportFragment = new SupportFragment();
                return supportFragment;
            case 3:
                ProfileFragment profileFragment=new ProfileFragment();
                return profileFragment;
            default:
                return null;
        }
    }
    // this counts total number of tabs
    @Override
    public int getCount() {
        return totalTabs;
    }
}