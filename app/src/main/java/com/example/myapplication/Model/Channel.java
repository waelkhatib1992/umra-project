package com.example.myapplication.Model;

import android.graphics.drawable.Drawable;

public class Channel {
    private Drawable drawable;

    public Channel(Drawable drawable, String title) {
        this.drawable = drawable;
        this.title = title;
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }

    private String title;

    public Channel(Drawable drawable, String title, String sub_title) {
        this.drawable = drawable;
        this.title = title;
        this.sub_title = sub_title;
    }

    public String getSub_title() {
        return sub_title;
    }

    public void setSub_title(String sub_title) {
        this.sub_title = sub_title;
    }

    private String sub_title;



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
