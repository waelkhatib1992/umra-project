package com.example.myapplication.Views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.core.widget.TextViewCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.app.DatePickerDialog;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.Adapters.RecycleAdapter;
import com.example.myapplication.Adapters.ViewPageAdapter;
import com.example.myapplication.Model.City;
import com.example.myapplication.R;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity {
private List<City> source;
    TabLayout tabLayout;
    ViewPager viewPager;
    public static final int[] tabIcon = {R.drawable.home, R.drawable.packages, R.drawable.support,R.drawable.profile};
    public static final String[] titles = {"Home","Packages","Support","Profile"};
//
//    private void setCustomTabs() {
//
//        for (int i = 0; i < tabIcon.length; i++) {
//            View view = getLayoutInflater().inflate(R.layout.tab_icon,null);
//            TabLayout.Tab tab = tabLayout.getTabAt(i);
//            view.findViewById(R.id.icon).setBackgroundResource(tabIcon[i]);
//            if(tab!=null) tab.setCustomView(view);
//        }
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        RecyclerView recyclerView
                = (RecyclerView)findViewById(
                R.id.recyv);
       LinearLayoutManager RecyclerViewLayoutManager
                = new LinearLayoutManager(
                getApplicationContext());

        // Set LayoutManager on Recycler View
        recyclerView.setLayoutManager(
                RecyclerViewLayoutManager);

        // Adding items to RecyclerView.
        AddItemsToRecyclerViewArrayList();

        // calling constructor of adapter
        // with source list as a parameter
      RecycleAdapter adapter = new RecycleAdapter(source);

        // Set Horizontal Layout Manager
        // for Recycler view
        LinearLayoutManager HorizontalLayout
                = new LinearLayoutManager(
                MainActivity.this,
                LinearLayoutManager.HORIZONTAL,
                false);
        recyclerView.setLayoutManager(HorizontalLayout);

        // Set adapter on recycler view
        recyclerView.setAdapter(adapter);
        tabLayout=(TabLayout)findViewById(R.id.tabLayout);
        viewPager=(ViewPager)findViewById(R.id.viewPager);
        for (int i = 0; i < tabIcon.length; i++) {
            View view = getLayoutInflater().inflate(R.layout.tab_icon,null);

            view.findViewById(R.id.icon).setBackgroundResource(tabIcon[i]);
            ((TextView)view.findViewById(R.id.title)).setText(titles[i]);
            if(i==0){
                ((TextView)view.findViewById(R.id.title)).setTextColor(getResources().getColor(R.color.green_text_color));
            }
            tabLayout.addTab(tabLayout.newTab().setCustomView(view));
        }
//
//        tabLayout.addTab(tabLayout.newTab().setText("Home").setIcon(R.drawable.home));
//        tabLayout.addTab(tabLayout.newTab().setText("Packages").setIcon(R.drawable.packages));
//        tabLayout.addTab(tabLayout.newTab().setText("Support").setIcon(R.drawable.support));
//        tabLayout.addTab(tabLayout.newTab().setText("Profile").setIcon(R.drawable.profile));
//        int tabIconColor = ContextCompat.getColor(getApplicationContext(), R.color.green_text_color);
//        tabLayout.getTabAt(0).getIcon().set.setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPageAdapter adapter1 = new ViewPageAdapter(this,getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter1);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                View view =tab.getCustomView();
                ((TextView)view.findViewById(R.id.title)).setTextColor(getResources().getColor(R.color.green_text_color));
                ViewCompat.setBackgroundTintList(view.findViewById(R.id.icon),getResources().getColorStateList(R.color.green_text_color));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                View view =tab.getCustomView();
                ((TextView)view.findViewById(R.id.title)).setTextColor(getResources().getColor(R.color.grey_icon));
                ViewCompat.setBackgroundTintList(view.findViewById(R.id.icon),getResources().getColorStateList(R.color.grey_icon));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    // Function to add items in RecyclerView.
    public void AddItemsToRecyclerViewArrayList()
    {
        // Adding items to ArrayList
        source = new ArrayList<>();
        source.add(new City("Madinah",22));
        source.add(new City("Al-Riadh",26));
        source.add(new City("Mekkah",44));
        source.add(new City("Jeddah",52));

        source.add(new City("Madinah",22));
        source.add(new City("Al-Riadh",26));
        source.add(new City("Mekkah",44));
        source.add(new City("Jeddah",52));

        source.add(new City("Madinah",22));
        source.add(new City("Al-Riadh",26));
        source.add(new City("Mekkah",44));
        source.add(new City("Jeddah",52));

        source.add(new City("Madinah",22));
        source.add(new City("Al-Riadh",26));
        source.add(new City("Mekkah",44));
        source.add(new City("Jeddah",52));

        source.add(new City("Madinah",22));
        source.add(new City("Al-Riadh",26));
        source.add(new City("Mekkah",44));
        source.add(new City("Jeddah",52));

        source.add(new City("Madinah",22));
        source.add(new City("Al-Riadh",26));
        source.add(new City("Mekkah",44));
        source.add(new City("Jeddah",52));

        source.add(new City("Madinah",22));
        source.add(new City("Al-Riadh",26));
        source.add(new City("Mekkah",44));
        source.add(new City("Jeddah",52));

        source.add(new City("Madinah",22));
        source.add(new City("Al-Riadh",26));
        source.add(new City("Mekkah",44));
        source.add(new City("Jeddah",52));

        source.add(new City("Madinah",22));
        source.add(new City("Al-Riadh",26));
        source.add(new City("Mekkah",44));
        source.add(new City("Jeddah",52));

        source.add(new City("Madinah",22));
        source.add(new City("Al-Riadh",26));
        source.add(new City("Mekkah",44));
        source.add(new City("Jeddah",52));

        source.add(new City("Madinah",22));
        source.add(new City("Al-Riadh",26));
        source.add(new City("Mekkah",44));
        source.add(new City("Jeddah",52));

    }
    public void changeStyle(View view) {
        if(view.getId()==R.id.opt1)
            makeChange(R.id.opt1,R.id.opt2);
        else
            makeChange(R.id.opt2,R.id.opt1);

    }

    private void makeChange(int opt1, int opt2) {
TextView t1=((TextView)findViewById(opt1));
TextView t2=((TextView)findViewById(opt2));

        TextViewCompat.setTextAppearance(t1,R.style.fillTextStyle);
        TextViewCompat.setTextAppearance(t2,R.style.outlineTextStyle);
        ViewCompat.setBackgroundTintList(t2,getResources().getColorStateList(android.R.color.transparent));
        ViewCompat.setBackgroundTintList(t1,getResources().getColorStateList(R.color.green_text_color));
        t1.setBackground(ContextCompat.getDrawable( this,R.drawable.rounded_shape));


    }

    public void openDatePicker(View view) {
        EditText e=(EditText)view;
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
        // date picker dialog
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        // set day of month , month and year value in the edit text
                        e.setText(dayOfMonth + "/"
                                + (monthOfYear + 1) + "/" + year);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }


    public void openSelectionMenu(View view) {
        Toast.makeText(this,"Bottom sheet should be shown",Toast.LENGTH_SHORT).show();
    }
}
