package com.example.myapplication.Views;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myapplication.Adapters.ChannelsAdapter;
import com.example.myapplication.Adapters.NewsAdapter;
import com.example.myapplication.Adapters.RecycleAdapter;
import com.example.myapplication.Model.Channel;
import com.example.myapplication.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
View view=inflater.inflate(R.layout.fragment_home, container, false);

        RecyclerView recyclerView1
                = (RecyclerView)view. findViewById(
                R.id.recyv1);
        LinearLayoutManager RecyclerViewLayoutManager1
                = new LinearLayoutManager(
                getActivity());

        // Set LayoutManager on Recycler View
        recyclerView1.setLayoutManager(
                RecyclerViewLayoutManager1);

        // Adding items to RecyclerView.
        // calling constructor of adapter
        // with source list as a parameter
        List<Channel> channels1=new ArrayList<>();
        channels1.add(new Channel(getActivity().getResources().getDrawable(R.drawable.b1),"Latest news","lorem ipsox lorem ipsox lorem ipsox lorem ipsox lorem ipsox lorem ipsox lorem ipsox "));
        channels1.add(new Channel(getActivity().getResources().getDrawable(R.drawable.a1),"Latest news","lorem ipsox lorem ipsox lorem ipsox lorem ipsox lorem ipsox lorem ipsox lorem ipsox "));
        NewsAdapter adapter1 = new NewsAdapter(channels1);

        // Set Horizontal Layout Manager
        // for Recycler view
        LinearLayoutManager VerticallLayout
                = new LinearLayoutManager(
                getActivity(),
                LinearLayoutManager.VERTICAL,
                false){
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        recyclerView1.setLayoutManager(VerticallLayout);

        // Set adapter on recycler view
        recyclerView1.setAdapter(adapter1);

        RecyclerView recyclerView
                = (RecyclerView)view. findViewById(
                R.id.recyv);
        LinearLayoutManager RecyclerViewLayoutManager
                = new LinearLayoutManager(
                getActivity());

        // Set LayoutManager on Recycler View
        recyclerView.setLayoutManager(
                RecyclerViewLayoutManager);

        // Adding items to RecyclerView.
        // calling constructor of adapter
        // with source list as a parameter
        List<Channel> channels=new ArrayList<>();
        channels.add(new Channel(getActivity().getResources().getDrawable(R.drawable.b2),"Makkah live"));
        channels.add(new Channel(getActivity().getResources().getDrawable(R.drawable.a2),"AlMadinah live"));
        ChannelsAdapter adapter = new ChannelsAdapter(channels);

        // Set Horizontal Layout Manager
        // for Recycler view
        LinearLayoutManager HorizontalLayout
                = new LinearLayoutManager(
                getActivity(),
                LinearLayoutManager.HORIZONTAL,
                false);
        recyclerView.setLayoutManager(HorizontalLayout);

        // Set adapter on recycler view
        recyclerView.setAdapter(adapter);


        return view;
    }
}